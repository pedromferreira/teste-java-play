# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cliente (
  nome                      varchar(255) not null,
  tipo_pessoa               integer,
  constraint ck_cliente_tipo_pessoa check (tipo_pessoa in (0,1)),
  constraint pk_cliente primary key (nome))
;

create table fornecedor (
  nome                      varchar(255) not null,
  tipo_pessoa               integer,
  constraint ck_fornecedor_tipo_pessoa check (tipo_pessoa in (0,1)),
  constraint pk_fornecedor primary key (nome))
;

create sequence cliente_seq;

create sequence fornecedor_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists cliente;

drop table if exists fornecedor;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists cliente_seq;

drop sequence if exists fornecedor_seq;

