# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cliente (
  nome                      varchar(255) not null,
  tipo_pessoa               integer,
  constraint ck_cliente_tipo_pessoa check (tipo_pessoa in (0,1)),
  constraint pk_cliente primary key (nome))
;

create table fornecedor (
  nome                      varchar(255) not null,
  tipo_pessoa               integer,
  constraint ck_fornecedor_tipo_pessoa check (tipo_pessoa in (0,1)),
  constraint pk_fornecedor primary key (nome))
;




# --- !Downs

drop table if exists cliente cascade;

drop table if exists fornecedor cascade;

