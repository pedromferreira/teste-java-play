package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;

@Entity
public class Fornecedor extends Model {

	@Id
	public String nome;

	public TipoPessoa tipoPessoa;

	public static Finder<String, Fornecedor> find = new Finder<String, Fornecedor>(
			Fornecedor.class);

	public static List<Fornecedor> findAll() {
		return Fornecedor.find.all();
	}

	public static Fornecedor save(Fornecedor fornecedor) {
		if(Fornecedor.find.byId(fornecedor.nome) != null){
			fornecedor.update();
		}else{
			fornecedor.save();	
		}
		return fornecedor;
	}

	public static void remover(String nome) {
		Fornecedor.find.ref(nome).delete();
	}

	public static Fornecedor editar(String nome) {
		return Fornecedor.find.byId(nome);
	}
}
