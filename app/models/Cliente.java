package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;

@Entity
public class Cliente extends Model {

	@Id
	public String nome;

	public TipoPessoa tipoPessoa;

	public static Finder<String, Cliente> find = new Finder<String, Cliente>(Cliente.class);

	public static List<Cliente> findAll() {
		return Cliente.find.all();
	}
	
	public static Cliente save(Cliente cliente){
		if(Cliente.find.byId(cliente.nome) != null){
			cliente.update();
		}else{
			cliente.save();	
		}
		return cliente;
	}
	
	public static void remover(String nome){
		Cliente.find.ref(nome).delete();
	}
	
	public static Cliente editar(String nome){
		return Cliente.find.byId(nome);
	}
}
