package controllers;

import java.util.List;

import models.Fornecedor;
import play.*;
import play.data.Form;
import play.mvc.*;
import views.html.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class FornecedorController extends Controller {

	Form<Fornecedor> fornecedorForm = Form.form(Fornecedor.class);

	public Result index() {
		List<Fornecedor> fornecedores = Fornecedor.findAll();
		return ok(views.html.fornecedor.fornecedores.render("fornecedores",
				fornecedores));
	}

	public Result novo() {
		return ok(views.html.fornecedor.novo.render("fornecedores",
				fornecedorForm));
	}

	public Result editar(String nome) {
		Fornecedor fornecedor = Fornecedor.editar(nome);
		return ok(views.html.fornecedor.novo.render("fornecedores",
				fornecedorForm.fill(fornecedor)));
	}

	public Result remover(String nome) {
		Fornecedor.remover(nome);
		return redirect(routes.FornecedorController.index());
	}

	public Result save() {
		play.mvc.Http.MultipartFormData body = request().body()
				.asMultipartFormData();
		play.mvc.Http.MultipartFormData.FilePart arquivo = body
				.getFile("arquivo");
		if (arquivo != null) {
			if (fornecedorForm.bindFromRequest().hasErrors()) {
				return badRequest("ops");
			} else {
				Fornecedor fornecedor = fornecedorForm.bindFromRequest().get();
				Fornecedor.save(fornecedor);

				File file = new File("fornecedores/" + fornecedor.nome);
				try {
					BufferedWriter bw = new BufferedWriter(new FileWriter(file));
					BufferedReader br = new BufferedReader(
							new InputStreamReader(new FileInputStream(
									arquivo.getFile())));

					String aLine = null;
					while ((aLine = br.readLine()) != null) {
						// Process each line and add output to Dest.txt file
						bw.write(aLine);
						bw.newLine();
					}

					bw.close();
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

				return redirect(routes.FornecedorController.index());
			}
		} else {
			return badRequest("colocar arquivo");
		}
	}

}
