package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import play.*;
import play.data.Form;
import play.mvc.*;
import models.*;
import views.html.*;

public class ClienteController extends Controller {

	Form<Cliente> clienteForm = Form.form(Cliente.class);
	
    public Result index() {
    	List<Cliente> clientes= Cliente.findAll();
    	
    	return ok(views.html.cliente.clientes.render("clientes",clientes));
    }
    
    public Result novo() {
    	return ok(views.html.cliente.novo.render("clientes",clienteForm));
    }
    
    public Result editar(String nome) {
    	Cliente cliente = Cliente.editar(nome);
    	return ok(views.html.cliente.novo.render("clientes",clienteForm.fill(cliente)));
    }
    public Result remover(String nome) {
		Cliente.remover(nome);
		return redirect(routes.ClienteController.index());
	}

    public Result save() {
    	if (clienteForm.bindFromRequest().hasErrors()) {
    	    return badRequest("ops");
    	} else {
    		Cliente cliente = clienteForm.bindFromRequest().get();
    		Cliente.save(cliente);
    	    return redirect(routes.ClienteController.index());
    	}
    }
}
